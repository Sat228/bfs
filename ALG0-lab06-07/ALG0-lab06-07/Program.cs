﻿using ALG0_lab06_07;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ALG0_lab06_07
{
    class Program
    {
        static void Main(string[] args)
        {
            var n01 = new Node("Киев");
            var n02 = new Node("Житомир");
            var n03 = new Node("Новоград-Волынский");
            var n04 = new Node("Ровно");
            var n05 = new Node("Луцк");
            var n06 = new Node("Бердичев");
            var n07 = new Node("Винница");
            var n08 = new Node("Хмельницкий");
            var n09 = new Node("Тернополь");
            var n10 = new Node("Шепетовка");
            var n11 = new Node("Белая церковь");
            var n12 = new Node("Умань");
            var n13 = new Node("Черкассы");
            var n14 = new Node("Кременчуг");
            var n15 = new Node("Полтава");
            var n16 = new Node("Харьков");
            var n17 = new Node("Прилуки");
            var n18 = new Node("Сумы");
            var n19 = new Node("Миргород");
            n01.AddChildren(n02).AddChildren(n11).AddChildren(n17);
            n02.AddChildren(n10).AddChildren(n06).AddChildren(n03);
            n03.AddChildren(n04);
            n04.AddChildren(n05);
            n06.AddChildren(n07);
            n07.AddChildren(n08);
            n08.AddChildren(n09);
            n11.AddChildren(n12).AddChildren(n13).AddChildren(n15);
            n13.AddChildren(n14);
            n15.AddChildren(n16);
            n17.AddChildren(n18).AddChildren(n19);

            int caseSwitch;
            Console.WriteLine("1 - DFS");
            Console.WriteLine("2 - DLS");
            Console.WriteLine("3 - BLS");
            Console.WriteLine("3 - Все маршруты с Киева");
            bool Ban1;
            do
            {
                Console.Write("Введіть номер завдання: ");
                Ban1 = int.TryParse(Console.ReadLine(), out caseSwitch);
                if (!Ban1)
                    Console.WriteLine("  Помилка введенний номер. Будь-ласка повторіть введення значення ще раз!");
            }
            while (!Ban1);
            switch(caseSwitch)
            {
                case 1: //DFS
                    var search_DFS = new DepthFirstSearch();
                    var path_DFS = search_DFS.DFS(n09, n16);
                    PrintPath_DFS(path_DFS);

                    path_DFS = search_DFS.DFS(n09, n16);
                    PrintPath_DFS(path_DFS); // You shall not pass!
                    break;
                case 2://DLS                   
                    var search_DLS = new DepthLinkedSearch();
                    var path_DLS = search_DLS.DLS(n10, n18, 4);
                    PrintPath_DLS(path_DLS);

                    path_DLS = search_DLS.DLS(n10, n18, 4);
                    PrintPath_DLS(path_DLS); // You shall not pass! - Не хватает лимита глубины.
                    break;
                case 3://BLS                   
                    break;
                case 4:
                    var search_ALL = new DepthFirstSearch();
                    var path_ALL = search_ALL.DFS(n01, n09);
                    PrintPath_DFS(path_ALL);

                    path_ALL = search_ALL.DFS(n01, n19);
                    PrintPath_DFS(path_ALL);
                    path_ALL = search_ALL.DFS(n01, n18);
                    PrintPath_DFS(path_ALL);
                    path_ALL = search_ALL.DFS(n01, n05);
                    PrintPath_DFS(path_ALL);
                    path_ALL = search_ALL.DFS(n01, n14);
                    PrintPath_DFS(path_ALL);
                    path_ALL = search_ALL.DFS(n01, n16);
                    PrintPath_DFS(path_ALL);
                    path_ALL = search_ALL.DFS(n01, n12);
                    PrintPath_DFS(path_ALL);
                    break;
            }
        }
        private static void PrintPath_DFS(LinkedList<Node> path_DFS)
        {
            Console.WriteLine();
            if (path_DFS.Count == 0)
            {
                Console.WriteLine("You shall not pass!");
            }
            else
            {
                Console.WriteLine(string.Join(" -> ", path_DFS.Select(x => x.Name)));
            }
            Console.Read();
        }
        private static void PrintPath_DLS(LinkedList<Node> path_DLS)
        {
            Console.WriteLine();
            if (path_DLS.Count == 0)
            {
                Console.WriteLine("You shall not pass!");
            }
            else
            {
                Console.WriteLine(string.Join(" -> ", path_DLS.Select(x => x.Name)));
            }
            Console.Read();
        }
        private static void PrintPath_BFS(LinkedList<Node> path_BFS)
        {
            Console.WriteLine();
            if (path_BFS.Count == 0)
            {
                Console.WriteLine("You shall not pass!");
            }
            else
            {
                Console.WriteLine(string.Join(" -> ", path_BFS.Select(x => x.Name)));
            }
            Console.Read();
        }
    }

    class Node
    {
        public string Name { get; }
        public List<Node> Children { get; }

        public Node(string name)
        {
            Name = name;
            Children = new List<Node>();
        }

        public Node AddChildren(Node node, bool bidirect = true)
        {
            Children.Add(node);
            if (bidirect)
            {
                node.Children.Add(this);
            }
            return this;
        }

        public void Handler()
        {
            Console.WriteLine($"visited {this.Name}");
        }
    }
}
