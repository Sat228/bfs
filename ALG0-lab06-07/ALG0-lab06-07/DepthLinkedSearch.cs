﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALG0_lab06_07
{
    class DepthLinkedSearch
    {
        private HashSet<Node> visited;
        private LinkedList<Node> path_DLS;
        private Node goal;
        private bool limitWasReached;
        public LinkedList<Node> DLS(Node start, Node goal, int limit)
        {
            visited = new HashSet<Node>();
            path_DLS = new LinkedList<Node>();
            limitWasReached = true;
            this.goal = goal;
            DLS(start, limit);
            if (path_DLS.Count > 0)
            {
                path_DLS.AddFirst(start);
            }
            return path_DLS;
        }

        private bool DLS(Node node, int limit)
        {
            node.Handler();
            if (node == goal)
            {
                return true;
            }
            if (limit == 0)
            {
                limitWasReached = false;
                return false;
            }
            visited.Add(node);
            foreach (var child in node.Children.Where(x => !visited.Contains(x)))
            {
                if (DLS(child, limit - 1))
                {
                    path_DLS.AddFirst(child);
                    return true;
                }
            }
            return false;
        }
    }
}