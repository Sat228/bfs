﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALG0_lab06_07
{
    class BreadthFirstSearch
    {
        private HashSet<Node> visited;
        private LinkedList<Node> path_BFS;
        private Node goal;
    }
    public LinkedList<Node> BFS(Node start, Node goal)
    {
        visited = new HashSet<Node>();
        path_BFS = new LinkedList<Node>();
        this.goal = goal;
        BFS(start);
        if (path_BFS.Count > 0)
        {
            path_BFS.AddFirst(start);
        }
        return path_BFS;
    }

    private bool BFS(Node node)
    {
        node.Handler();
        if (node == goal)
        {
            return true;
        }
        visited.Add(node);
        foreach (var child in node.Children.Where(x => !visited.Contains(x)))
        {
            if (BFS(child))
            {
                path_BFS.AddFirst(child);
                return true;
            }
        }
        return false;
    }

    internal object BFS(Node n03, Node n11, int v)
    {
        throw new NotImplementedException();
    }
}